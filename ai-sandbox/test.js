// hello worldconsole.log('hello world');   
function printName(name) {
  console.log(name);
}

printName('John');


// fetch list of hotels from API
async function getHotels() {
  try {
    const response = await fetch('/hotels');
    return await response.json();
  } catch (error) {
    console.error('Failed to get hotels', error);
    return [];
  }
}

